<?php 
 //Importing Database Script 
 require_once('dbConnect.php');
 
 $tm = time();
 //Creating sql query
 $sql = "SELECT * FROM testdetail";
 
 //getting result 
 $r = mysqli_query($con,$sql);
 
 //creating a blank array 
 $result = array();
 
 //looping through all the records fetched
 while($row = mysqli_fetch_array($r)){
 
 //Pushing name and id in the blank array created 
 $date = strtotime($row['edittime']);
 $currenttime = date('d-F-Y',$date);

 array_push($result,array(
 "testid"=>$row['testid'],
 "testname"=>$row['testname'],
 "questions"=>$row['questions'],
 "marks"=>$row['marks'],
 "time"=>$currenttime,
  ));
 }
 
 //Displaying the array in json format 
 echo json_encode(array('result'=>$result));
 
 mysqli_close($con);
<?php 
 //Importing Database Script 
 require_once('dbConnect.php');
 
  $testid = $_GET['testid'];

 //Creating sql query
 $sql = "SELECT * FROM result";
 
 //getting result 
 $r = mysqli_query($con,$sql);
 
 //creating a blank array 
 $result = array();
 
 //looping through all the records fetched
 while($row = mysqli_fetch_array($r)){
 
 //Pushing name and id in the blank array created 
 array_push($result,array(
  "regno"=>$row['regno'],
  "name"=>$row['name'],
  "testid"=>$row['testid'],
  "scored"=>$row['scored'],
  ));
 }
 
 //Displaying the array in json format 
 echo json_encode(array('result'=>$result));
 
 mysqli_close($con);
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Authenticate</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="res/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="res/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="res/vendor/animate/animate.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="res/vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="res/vendor/perfect-scrollbar/perfect-scrollbar.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="res/css/util.css">
	<link rel="stylesheet" type="text/css" href="res/css/main.css">
<!--===============================================================================================-->

</head>
<body >
	
	<div class="limiter">
		<div class="container-table100">
			<div class="wrap-table100">
				<div class="table100">
				<?php
                    // Include config file
                    require_once 'res/config.php';
                    
                    // Attempt select query execution
                    $sql = "SELECT * FROM register";
                    if($result = mysqli_query($link, $sql)){
                        if(mysqli_num_rows($result) > 0){
						echo "<table>";
						echo "<thead>";
						echo "<tr class='table100-head'>";
							echo "<th class='column1'>Registration No</th>";
							echo "<th class='column2'>Name</th>";
							echo "<th class='column3'>Department</th>";
							echo "<th class='column4'>Year</th>";
							echo "<th class='column5'>Email</th>";
							echo "<th class='column6'>Mobile No</th>";
							echo "<th class='column7'>Action</th>";
							echo "<th class='column7'>Action</th>";
						echo "</tr>";
						echo "</thead>";
						while($row = mysqli_fetch_array($result)){
							echo "<tbody>";
								echo "<tr>";
								echo "<td class='column1'>". $row['regno'] ."</td>";
								echo "<td class='column2'>". $row['name'] ."</td>";
								echo "<td class='column3'>". $row['dept'] ."</td>";
								echo "<td class='column4'>". $row['year'] ."</td>";
								echo "<td class='column5'>". $row['email'] ."</td>";
								echo "<td class='column6'>". $row['mobno'] ."</td>";
								echo "<td class='column7'>";
								echo "<a href='res/auth.php?id=". $row['regno'] ."' title='Authenticate user' data-toggle='tooltip'><span class='glyphicon glyphicon-ok-circle'>Accept</span></a>";
								echo "</td>";
								echo "<td class='column7'>";
								echo "<a href='res/delete.php?id=". $row['regno'] ."' title='Unauthenticate user' data-toggle='tooltip'><span class='glyphicon glyphicon-ban-circle'>Delete</span></a>";
								echo "</td>";
								echo "</tr>";
							}
							echo "</tbody>";                            
						echo "</table>";
						// Free result set
						mysqli_free_result($result);
					} else{
						echo "<p class='lead'><em>No records were found.</em></p>";
					}
				} else{
					echo "ERROR: Could not able to execute $sql. " . mysqli_error($link);
				}

				// Close connection
				mysqli_close($link);
				?>
				</div>
			</div>
		</div>
	</div>


	

<!--===============================================================================================-->	
	<script src="res/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="res/vendor/bootstrap/js/popper.js"></script>
	<script src="res/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="res/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="res/js/main.js"></script>
	<footer><div id="foot">Copyright &copy; Sasurie InfoTech</div></footer>

</body>
</html>
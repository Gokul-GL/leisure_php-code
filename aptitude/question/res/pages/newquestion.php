<?php
	// Include config file
    require_once 'config.php';
    $teid = $_GET["tid"];
    $add = $_GET["test"];
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Admin Panel</title>

        <script>
            function generateRow() {
                var d=document.getElementById("div");
                d.innerHTML+="<p><label><input type='text' name='ques[]' id='que' placeholder='Question' required/>&nbsp"+
                "<input name='opt1[]' type='text' id='op' placeholder='Option1' required/>&nbsp;"+
                "<input name='opt2[]' type='text' id='op' placeholder='Option2' required/>&nbsp;"+
                "<input name='opt3[]' type='text' id='op' placeholder='Option3' required/>&nbsp;"+
                "<input name='opt4[]' type='text' id='op' placeholder='Option4' required/>&nbsp;&nbsp;"+
                "<input name='ans[]' type='text' id='an' placeholder='Answer' required/></label></p>";
            }
        </script>
        <link rel="stylesheet" type="text/css" href="../css/style.css">

        <!-- Bootstrap Core CSS -->
        <link href="../css/bootstrap.min.css" rel="stylesheet">

        <!-- MetisMenu CSS -->
        <link href="../css/metisMenu.min.css" rel="stylesheet">

        <!-- Timeline CSS -->
        <link href="../css/timeline.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="../css/startmin.css" rel="stylesheet">

        <!-- Morris Charts CSS -->
        <link href="../css/morris.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>

        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                     
                <ul class="nav navbar-nav navbar-left navbar-top-links">
                    <li><a href="//www.sasurie.com"><i class="fa fa-home fa-fw"></i> Website</a></li>
                </ul>
                    
                <ul class="nav navbar-nav navbar-right navbar-top-links">
      
                            <li class="divider"><a class="navbar-brand" >Admin</a></li>
                            <li><a href="logout.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                      
                       
                </ul>
                <!-- /.navbar-top-links -->

                <div class="navbar-default sidebar" role="navigation">
                    <div class="sidebar-nav navbar-collapse">
                        <ul class="nav" id="side-menu">
                            <li class="sidebar-search">
                               <div class="input-group custom-search-form">
                                    <img src="../images/logo.png">
                                </div>
                                <!-- /input-group -->
                            </li>
                            
                            <li>
                                <a href="index.php"><i class="fa fa-laptop fa-fw"></i> Dashboard</a>
                            </li>
                            <li>
                                <a href="tables.php?tab=question&amp;id=<?php echo NULL;?>"><i class="fa fa-question-circle-o fa-fw"></i> Question</a>
                            </li>
                            <li>
                                <a href="tables.php?tab=testdetail&amp;id=<?php echo NULL;?>"><i class="fa fa-pencil-square-o fa-fw"></i> Tests</a>
                            </li>
                            <li>
                                <a href="tables.php?tab=result&amp;id=<?php echo NULL;?>"><i class="fa fa-hand-o-up fa-fw"></i> Results</a>
                            </li>
                            <li>
                                <a href="tables.php?tab=login&amp;id=<?php echo NULL;?>"><i class="fa fa-users fa-fw"></i> Users</a>
                            </li>
                            <li>
                                <a href="viewdetail.php?view=request"><i class="fa fa-registered fa-fw"></i> New Request</a>
                            </li>
                            <li>
                                <a href="tables.php?tab=addtest&amp;id=<?php echo NULL;?>"><i class="fa fa-plus-square-o fa-fw"></i> Add Test</a>
                            </li>
                            <li>
                                <a href="update.php"><i class="fa fa-upload fa-fw"></i> Update</a>
                            </li>
                           
                        </ul>
                    </div>
                </div>
            </nav>
            <?php
            if($teid!=NULL){
                 $sql1 = "SELECT * FROM testdetail where testid = $teid";
                if($result1 = mysqli_query($con, $sql1)){
                    if(mysqli_num_rows($result1) > 0){
                        while($row1 = mysqli_fetch_array($result1)){
                            $testname = $row1['testname']; 
                        }
                    }
                }
                echo "<form id='form1' name='form1' method='post' action='newquestion.php?test=addquestion&tid=".NULL."'>";
                    
                        echo "<div id='page-wrapper'>
                                <div class='row'>
                                    <!-- /.col-lg-12 -->
                                    <div class='col-lg-12'>
                                        <h1 class='page-header'>Add Question to ".$testname." </h1>
                                    </div>
                                </div>
                                <!-- /.row -->
                                <div class='row'>
                                    <div class='col-lg-12'>";
                                    echo "<div class='panel panel-default'>
                                        <div class='panel-heading'>No of Question <div class='navbar-right'><button type='button' id='tessubmit' onclick='history.go(-1);'>Back </button></div></div>
                                        <!-- /.panel-heading -->
                                        <div class='panel-body'>
                                            <div class='dataTable_wrapper'>
                                                <table class='testtable table-striped table-bordered table-hover' >";
                                                echo "<table class='table table-striped table-bordered table-hover' >
                                                <input name='testid' type='hidden' value='".$teid."'/>
                                                <input name='testName' type='hidden' value='".$testname."'/>
                                                <p><label><input name='q' type='text' id='an' placeholder='Enter no question'required/>
                                                <input type='submit' name='submit' id='submit' value='Submit' />
                                                </label></p>
                                                </table>
                                            </div>";
                                        echo "</div>";
                                    echo "</div>";
                                echo "</div>";
                            echo "</div>";
                        echo "</div>";    
                 
                echo "</form>";
                }
                elseif($add=="addquestion"){
                echo "<form  method='post' action='store.php'>";
                    echo "<div id='page-wrapper'>
                                <div class='row'>
                                    <!-- /.col-lg-12 -->
                                    <div class='col-lg-12'>
                                        <h1 class='page-header'>Add Question to ".strtoupper($_POST['testName'])."</h1>
                                    </div>
                                </div>
                                <!-- /.row -->
                                <div class='row'>
                                    <div class='col-lg-12'>";
                                        echo "<div class='panel panel-default'>
                                            <div class='panel-heading'>New Questions <div class='navbar-right'><button type='button' id='tessubmit' onclick='history.go(-1);'>Back </button></div></div>
                                            <!-- /.panel-heading -->
                                            <div class='panel-body'>
                                                <div class='dataTable_wrapper'>
                                                    <table class='table table-striped table-bordered table-hover' >";
                                                    $id = $_POST["testid"];
                                                    echo "<input name='id' type='hidden' value='".$id."'/><br/>";
                                                    echo"<p><label><input type='text' name='ques[]' id='que' placeholder='Question' required/>&nbsp;
                                                        <input name='opt1[]' type='text' id='op' placeholder='Option1' required/>&nbsp;
                                                        <input name='opt2[]' type='text' id='op' placeholder='Option2' required/>&nbsp;
                                                        <input name='opt3[]' type='text' id='op' placeholder='Option3' required/>&nbsp;
                                                        <input name='opt4[]' type='text' id='op' placeholder='Option4' required/>&nbsp;
                                                        <input name='ans[]' type='text' id='an' placeholder='Answer' required/></label></p>";
                                                    echo "<div id='div'></div>";
                                                    $que = $_POST["q"];
                                                    for($i=0;$i<$que-1;$i++){
                                                        echo '<script type="text/javascript">',
                                                                'generateRow();',
                                                            '</script>';
                                                    }
                                                    echo "<br/><input type='submit' name='submit' id='submit' value='Submit' />
                                                    </p>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                </form>";
            }   
            ?>
                
        <!-- /#wrapper -->

        <!-- jQuery -->
        <script src="../js/jquery.min.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="../js/bootstrap.min.js"></script>

        <!-- Metis Menu Plugin JavaScript -->
        <script src="../js/metisMenu.min.js"></script>

        <!-- DataTables JavaScript -->
        <script src="../js/dataTables/jquery.dataTables.min.js"></script>
        <script src="../js/dataTables/dataTables.bootstrap.min.js"></script>

        <!-- Custom Theme JavaScript -->
        <script src="../js/startmin.js"></script>

        <!-- Page-Level Demo Scripts - Tables - Use for reference -->
        <script>
            $(document).ready(function() {
                $('#dataTables-example').DataTable({
                        responsive: true
                });
            });
        </script>

    </body>
</html>

<?php
	// Include config file
    require_once 'config.php';
    $views = $_GET["view"];
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Admin Panel</title>
        <link rel="stylesheet" type="text/css" href="../css/style.css">
        
        <!-- Bootstrap Core CSS -->
        <link href="../css/bootstrap.min.css" rel="stylesheet">

        <!-- MetisMenu CSS -->
        <link href="../css/metisMenu.min.css" rel="stylesheet">

        <!-- Timeline CSS -->
        <link href="../css/timeline.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="../css/startmin.css" rel="stylesheet">

        <!-- Morris Charts CSS -->
        <link href="../css/morris.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>

        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                     
                <ul class="nav navbar-nav navbar-left navbar-top-links">
                    <li><a href="//www.sasurie.com"><i class="fa fa-home fa-fw"></i> Website</a></li>
                </ul>
                    
                <ul class="nav navbar-nav navbar-right navbar-top-links">
      
                            <li class="divider"><a class="navbar-brand" >Admin</a></li>
                            <li><a href="logout.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                      
                       
                </ul>
                <!-- /.navbar-top-links -->

                <div class="navbar-default sidebar" role="navigation">
                    <div class="sidebar-nav navbar-collapse">
                        <ul class="nav" id="side-menu">
                            <li class="sidebar-search">
                               <div class="input-group custom-search-form">
                                    <img src="../images/logo.png">
                                </div>
                                <!-- /input-group -->
                            </li>
                            
                            <li>
                                <a href="index.php"><i class="fa fa-laptop fa-fw"></i> Dashboard</a>
                            </li>
                            <li>
                                <a href="tables.php?tab=question&amp;id=<?php echo NULL;?>"><i class="fa fa-question-circle-o fa-fw"></i> Question</a>
                            </li>
                            <li>
                                <a href="tables.php?tab=testdetail&amp;id=<?php echo NULL;?>"><i class="fa fa-pencil-square-o fa-fw"></i> Tests</a>
                            </li>
                            <li>
                                <a href="tables.php?tab=result&amp;id=<?php echo NULL;?>"><i class="fa fa-hand-o-up fa-fw"></i> Results</a>
                            </li>
                            <li>
                                <a href="tables.php?tab=login&amp;id=<?php echo NULL;?>"><i class="fa fa-users fa-fw"></i> Users</a>
                            </li>
                            <li>
                                <a href="viewdetail.php?view=request"><i class="fa fa-registered fa-fw"></i> New Request</a>
                            </li>
                            <li>
                                <a href="tables.php?tab=addtest&amp;id=<?php echo NULL;?>"><i class="fa fa-plus-square-o fa-fw"></i> Add Test</a>
                            </li>
                            <li>
                                <a href="update.php"><i class="fa fa-upload fa-fw"></i> Update</a>
                            </li>
                           
                        </ul>
                    </div>
                </div>
            </nav>
            <?php
            if($views=="todaytest"){
                echo "<div id='page-wrapper'>
                    <div class='row'>
                        <!-- /.col-lg-12 -->
                        <div class='col-lg-12'>
                            <h1 class='page-header'>Today's Test Taken</h1>
                        </div>
                    </div>
                    <!-- /.row -->
                    <div class='row'>
                        <div class='col-lg-12'>";
                            echo "<div class='panel panel-default'>
                                <div class='panel-heading'>User's <div class='navbar-right'><button type='button' id='tessubmit' onclick='history.go(-1);'>Back </button></div></div>
                                    <!-- /.panel-heading -->
                                    <div class='panel-body'>
                                        <div class='dataTable_wrapper'>
                                        <table class='table table-striped table-bordered table-hover'>";
                                       
                                        $date = date("Y-m-d");
                                        $sql = "SELECT * FROM result WHERE testtime LIKE '$date%'";
                                        if($result = mysqli_query($con, $sql)){
                                            if(mysqli_num_rows($result) > 0){
                                                $s = 0;
                                                 echo "<thead>";
                                                    echo "<tr>";
                                                        echo "<th>S No</th>";
                                                        echo "<th>Name</th>";
                                                        echo "<th>Test Name</th>";
                                                        echo "<th>Marks</th>";
                                                    echo "</tr>";
                                                echo "</thead>";
                                                echo "<tbody>";
                                                while($row = mysqli_fetch_array($result)){
                                                    $s +=1;
                                                    $reg = $row['regno'];
                                                    $id = $row['testid'];
                                                    $sql1 = "SELECT * FROM login WHERE regno = $reg";
                                                    if($result1 = mysqli_query($con, $sql1)){
                                                        if(mysqli_num_rows($result1) > 0){
                                                            while($row1 = mysqli_fetch_array($result1)){
                                                                $name = $row1['name'];
                                                            }
                                                            mysqli_free_result($result1);
                                                        }
                                                    }
                                                    $sql2 = "SELECT * FROM testdetail WHERE testid = $id";
                                                    if($result2 = mysqli_query($con, $sql2)){
                                                        if(mysqli_num_rows($result2) > 0){
                                                            while($row2 = mysqli_fetch_array($result2)){
                                                                $testname = $row2['testname'];
                                                            }
                                                            mysqli_free_result($result2);
                                                        }
                                                    }
                                                    echo "<tr class='odd gradeX'>";
                                                        echo "<td class='center'>". $s ."</td>";
                                                        echo "<td class='center'>". $name ."</td>";
                                                        echo "<td class='center'>". $testname ."</td>";
                                                        echo "<td class='center'>". $row['mark'] ."</td>";
                                                    echo "</tr>";
                                                }
                                                // Free result set
                                                mysqli_free_result($result);
                                            } else{
                                                
                                                echo "No one take test today";
                                                
                                        }
                                        }
                                       
                                        // Close connection
                                        mysqli_close($con);
                                        echo "</tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>";
            }
            else if($views=="request"){
                echo "<div id='page-wrapper'>
                    <div class='row'>
                        <!-- /.col-lg-12 -->
                        <div class='col-lg-12'>
                            <h1 class='page-header'>New Request's</h1>
                        </div>
                    </div>
                    <!-- /.row -->
                    <div class='row'>
                        <div class='col-lg-12'>";
                            echo "<div class='panel panel-default'>
                                <div class='panel-heading'>User's <div class='navbar-right'><button type='button' id='tessubmit' onclick='history.go(-1);'>Back </button></div></div>
                                    <!-- /.panel-heading -->
                                    <div class='panel-body'>
                                        <div class='dataTable_wrapper'>
                                        <table class='table table-striped table-bordered table-hover'>";
                                        echo "<thead>";
                                            echo "<tr>";
                                                echo "<th>S No</th>";
                                                echo "<th>Name</th>";
                                                echo "<th>Register No</th>";
                                                echo "<th>Department</th>";
                                                echo "<th>Year</th>";
                                            echo "</tr>";
                                        echo "</thead>";
                                        echo "<tbody>";
                                        $sql = "SELECT * FROM register";
                                        if($result = mysqli_query($con, $sql)){
                                            if(mysqli_num_rows($result) > 0){
                                                $s = 0;
                                                while($row = mysqli_fetch_array($result)){
                                                    $s +=1;
                                                    echo "<tr class='odd gradeX'>";
                                                        echo "<td class='center'>". $s ."</td>";
                                                        echo "<td class='center'>". $row['name'] ."</td>";
                                                        echo "<td class='center'>". $row['regno'] ."</td>";
                                                        echo "<td class='center'>". $row['dept'] ."</td>";
                                                        echo "<td class='center'>". $row['year'] ."</td>";
                                                    echo "</tr>";
                                                }
                                                // Free result set
                                                mysqli_free_result($result);
                                            }
                                        }
                                        // Close connection
                                        mysqli_close($con);
                                        echo "</tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>";
            }
            else if($views=="monthuser"){
                echo "<div id='page-wrapper'>
                    <div class='row'>
                        <!-- /.col-lg-12 -->
                        <div class='col-lg-12'>
                            <h1 class='page-header'>This Month added User's</h1>
                        </div>
                    </div>
                    <!-- /.row -->
                    <div class='row'>
                        <div class='col-lg-12'>";
                            echo "<div class='panel panel-default'>
                                <div class='panel-heading'>User's <div class='navbar-right'><button type='button' id='tessubmit' onclick='history.go(-1);'>Back </button></div></div>
                                    <!-- /.panel-heading -->
                                    <div class='panel-body'>
                                        <div class='dataTable_wrapper'>
                                        <table class='table table-striped table-bordered table-hover'>";
                                        echo "<thead>";
                                            echo "<tr>";
                                                echo "<th>S No</th>";
                                                echo "<th>Name</th>";
                                                echo "<th>Register No</th>";
                                                echo "<th>Department</th>";
                                                echo "<th>Year</th>";
                                            echo "</tr>";
                                        echo "</thead>";
                                        echo "<tbody>";
                                        $sql = "SELECT * FROM login WHERE MONTH(authtime) = MONTH(CURRENT_DATE()) AND YEAR(authtime) = YEAR(CURRENT_DATE())";
                                        if($result = mysqli_query($con, $sql)){
                                            if(mysqli_num_rows($result) > 0){
                                                $s = 0;
                                                while($row = mysqli_fetch_array($result)){
                                                    $s +=1;
                                                    echo "<tr class='odd gradeX'>";
                                                        echo "<td class='center'>". $s ."</td>";
                                                        echo "<td class='center'>". $row['name'] ."</td>";
                                                        echo "<td class='center'>". $row['regno'] ."</td>";
                                                        echo "<td class='center'>". $row['dept'] ."</td>";
                                                        echo "<td class='center'>". $row['year'] ."</td>";
                                                    echo "</tr>";
                                                }
                                                // Free result set
                                                mysqli_free_result($result);
                                            }
                                        }
                                        // Close connection
                                        mysqli_close($con);
                                        echo "</tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>";
            }
            else if($views=="monthquestion"){
                echo "<div id='page-wrapper'>
                    <div class='row'>
                        <!-- /.col-lg-12 -->
                        <div class='col-lg-12'>
                            <h1 class='page-header'>This Month added Question</h1>
                        </div>
                    </div>
                    <!-- /.row -->
                    <div class='row'>
                        <div class='col-lg-12'>";
                            echo "<div class='panel panel-default'>
                                <div class='panel-heading'>All questions <div class='navbar-right'><button type='button' id='tessubmit' onclick='history.go(-1);'>Back </button></div></div>
                                    <!-- /.panel-heading -->
                                    <div class='panel-body'>
                                        <div class='dataTable_wrapper'>
                                        <table class='table table-striped table-bordered table-hover'>";
                                        echo "<thead>";
                                            echo "<tr>";
                                                echo "<th>S No</th>";
                                                echo "<th>Question</th>";
                                                echo "<th>Choice 1</th>";
                                                echo "<th>Choice 2</th>";
                                                echo "<th>Choice 3</th>";
                                                echo "<th>Choice 4</th>";
                                                echo "<th>Answser</th>";
                                                echo "<th>Edit</th>";
                                                echo "<th>Delete</th>";
                                            echo "</tr>";
                                        echo "</thead>";
                                        echo "<tbody>";
                                        $sql = "SELECT * FROM question WHERE MONTH(addtime) = MONTH(CURRENT_DATE()) AND YEAR(addtime) = YEAR(CURRENT_DATE())";
                                        if($result = mysqli_query($con, $sql)){
                                            if(mysqli_num_rows($result) > 0){
                                                $s = 0;
                                                while($row = mysqli_fetch_array($result)){
                                                    $s +=1;
                                                    echo "<tr class='odd gradeX'>";
                                                        echo "<td class='center'>". $s ."</td>";
                                                        echo "<td class='center'>". $row['question'] ."</td>";
								                        echo "<td class='center'>". $row['ch1'] ."</td>";
								                        echo "<td class='center'>". $row['ch2'] ."</td>";
							        	                echo "<td class='center'>". $row['ch3'] ."</td>";
                                                        echo "<td class='center'>". $row['ch4'] ."</td>";
                                                        echo "<td class='center'>". $row['ans'] ."</td>";
                                                        echo "<td class='center'>";
								                            echo "<a href='tablestest.php?qid=". $row['sno'] ."&tid=".NULL."' title='Edit' data-toggle='tooltip'><span class='glyphicon glyphicon-ok-circle'>Edit</span></a>
                                                        </td>";
                                                        echo "<td class='center'>";
                                                            echo "<a href='deleteQuestion.php?id=". $row['sno'] ."' title='Delete' data-toggle='tooltip'><span class='glyphicon glyphicon-ban-circle'>Delete</span></a>
                                                        </td>";
                                                    echo "</tr>";
                                                }
                                                // Free result set
                                                mysqli_free_result($result);
                                            }
                                        }
                                        // Close connection
                                        mysqli_close($con);
                                        echo "</tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>";
            }
            else if($views=="monthtest"){
                echo "<div id='page-wrapper'>
                    <div class='row'>
                        <!-- /.col-lg-12 -->
                        <div class='col-lg-12'>
                            <h1 class='page-header'>This Month added Test Details</h1>
                        </div>
                    </div>
                    <!-- /.row -->
                    <div class='row'>
                        <div class='col-lg-12'>";
                            echo "<div class='panel panel-default'>
                                <div class='panel-heading'>All Test's <div class='navbar-right'><a href='tables.php?tab=addtest&id=".NULL."'><button type='button' id='tessubmit'>Add Test !</button></a>&nbsp;&nbsp;&nbsp;&nbsp;<button type='button' id='tessubmit' onclick='history.go(-1);'>Back </button></div></div>
                                    <!-- /.panel-heading -->
                                    <div class='panel-body'>
                                        <div class='dataTable_wrapper'>
                                        <table class='table table-striped table-bordered table-hover'>";
                                        echo "<thead>";
                                            echo "<tr>";
                                                echo "<th>S No</th>";
                                                echo "<th>Test Name</th>";
                                                echo "<th>Total Questions</th>";
                                                echo "<th>Edit</th>";
                                                echo "<th>Delete</th>";
                                            echo "</tr>";
                                        echo "</thead>";
                                        echo "<tbody>";
                                        $sql = "SELECT * FROM testdetail WHERE MONTH(edittime) = MONTH(CURRENT_DATE()) AND YEAR(edittime) = YEAR(CURRENT_DATE())";
                                        if($result = mysqli_query($con, $sql)){
                                            if(mysqli_num_rows($result) > 0){
                                                $s = 0;
                                                while($row = mysqli_fetch_array($result)){
                                                    $s +=1;
                                                    echo "<tr class='odd gradeX'>";
                                                        echo "<td class='center'>". $s ."</td>";
                                                        echo "<td class='center'>". $row['testname'] ."</td>";
							    	                    echo "<td class='center'>". $row['questions'] ."</td>";
                                                        echo "<td class='center'>";
						            		                echo "<a href='tablestest.php?tid=". $row['testid'] ."&qid=".NULL."' title='Edit' data-toggle='tooltip'><span class='glyphicon glyphicon-ok-circle'>Edit</span></a>
                                                        </td>";
                                                        echo "<td class='center'>";
                                                            echo "<a href='deleteTest.php?id=". $row['testid'] ."' title='Delete' data-toggle='tooltip'><span class='glyphicon glyphicon-ban-circle'>Delete</span></a>
                                                        </td>";
                                                    echo "</tr>";
                                                }
                                                // Free result set
                                                mysqli_free_result($result);
                                            }
                                        }
                                        // Close connection
                                        mysqli_close($con);
                                        echo "</tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>";
            }
            ?>
        <!-- jQuery -->
        <script src="../js/jquery.min.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="../js/bootstrap.min.js"></script>

        <!-- Metis Menu Plugin JavaScript -->
        <script src="../js/metisMenu.min.js"></script>

        <!-- DataTables JavaScript -->
        <script src="../js/dataTables/jquery.dataTables.min.js"></script>
        <script src="../js/dataTables/dataTables.bootstrap.min.js"></script>

        <!-- Custom Theme JavaScript -->
        <script src="../js/startmin.js"></script>

        <!-- Page-Level Demo Scripts - Tables - Use for reference -->
        <script>
            $(document).ready(function() {
                $('#dataTables-example').DataTable({
                        responsive: true
                });
            });
        </script>

    </body>
</html>

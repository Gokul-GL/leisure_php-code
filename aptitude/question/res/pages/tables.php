<?php
	// Include config file
    require_once 'config.php';
    $table = $_GET["tab"];
    $teid = $_GET["id"];

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Admin Panel</title>

        <script>
function generateRow() {

var d=document.getElementById("div");
d.innerHTML+="<p><label class='label'><input type='text' name='ques[]' id='que' placeholder='Question' required/>&nbsp"+
"<input name='opt1[]' type='text' id='op' placeholder='Option1' required/>&nbsp;"+
"<input name='opt2[]' type='text' id='op' placeholder='Option2' required/>&nbsp;"+
"<input name='opt3[]' type='text' id='op' placeholder='Option3' required/>&nbsp;"+
"<input name='opt4[]' type='text' id='op' placeholder='Option4' required/>&nbsp;&nbsp;"+
"<input name='ans[]' type='text' id='an' placeholder='Answer' required/></label></p>";
}

</script>

<link rel="stylesheet" type="text/css" href="../css/style.css">


        <!-- Bootstrap Core CSS -->
        <link href="../css/bootstrap.min.css" rel="stylesheet">

        <!-- MetisMenu CSS -->
        <link href="../css/metisMenu.min.css" rel="stylesheet">

        <!-- Timeline CSS -->
        <link href="../css/timeline.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="../css/startmin.css" rel="stylesheet">

        <!-- Morris Charts CSS -->
        <link href="../css/morris.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>

        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                     
                <ul class="nav navbar-nav navbar-left navbar-top-links">
                    <li><a href="//www.sasurie.com"><i class="fa fa-home fa-fw"></i> Website</a></li>
                </ul>
                    
                <ul class="nav navbar-nav navbar-right navbar-top-links">
      
                            <li class="divider"><a class="navbar-brand" >Admin</a></li>
                            <li><a href="logout.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                      
                       
                </ul>
                <!-- /.navbar-top-links -->

                <div class="navbar-default sidebar" role="navigation">
                    <div class="sidebar-nav navbar-collapse">
                        <ul class="nav" id="side-menu">
                            <li class="sidebar-search">
                               <div class="input-group custom-search-form">
                                    <img src="../images/logo.png">
                                </div>
                                <!-- /input-group -->
                            </li>
                            
                            <li>
                                <a href="index.php"><i class="fa fa-laptop fa-fw"></i> Dashboard</a>
                            </li>
                            <li>
                                <a href="tables.php?tab=question&amp;id=<?php echo NULL;?>"><i class="fa fa-question-circle-o fa-fw"></i> Question</a>
                            </li>
                            <li>
                                <a href="tables.php?tab=testdetail&amp;id=<?php echo NULL;?>"><i class="fa fa-pencil-square-o fa-fw"></i> Tests</a>
                            </li>
                            <li>
                                <a href="tables.php?tab=result&amp;id=<?php echo NULL;?>"><i class="fa fa-hand-o-up fa-fw"></i> Results</a>
                            </li>
                            <li>
                                <a href="tables.php?tab=login&amp;id=<?php echo NULL;?>"><i class="fa fa-users fa-fw"></i> Users</a>
                            </li>
                            <li>
                                <a href="viewdetail.php?view=request"><i class="fa fa-registered fa-fw"></i> New Request</a>
                            </li>
                            <li>
                                <a href="tables.php?tab=addtest&amp;id=<?php echo NULL;?>"><i class="fa fa-plus-square-o fa-fw"></i> Add Test</a>
                            </li>
                            <li>
                                <a href="update.php"><i class="fa fa-upload fa-fw"></i> Update</a>
                            </li>
                           
                        </ul>
                    </div>
                </div>
            </nav>
            <?php
            if($teid=="addtest1"){
                echo "<form  method='post' action='store.php'>";
                    echo "<div id='page-wrapper'>
                                <div class='row'>
                                    <!-- /.col-lg-12 -->
                                    <div class='col-lg-12'>
                                        <h1 class='page-header'>Add Question to ".strtoupper($_POST['testName'])."</h1>
                                    </div>
                                </div>
                                <!-- /.row -->
                                <div class='row'>
                                    <div class='col-lg-12'>";
                                        echo "<div class='panel panel-default'>
                                            <div class='panel-heading'>New Questions</div>
                                            <!-- /.panel-heading -->
                                            <div class='panel-body'>
                                                <div class='dataTable_wrapper'>
                                                    <table class='table table-striped table-bordered table-hover' >";
                                        $id = $_POST["id"];
                            echo "<input name='id' type='hidden' value='".$id."'/><br/>";
                            echo"<p><label class='label'><input type='text' name='ques[]' id='que' placeholder='Question' required/>&nbsp;
                            <input name='opt1[]' type='text' id='op' placeholder='Option1' required/>&nbsp;
                            <input name='opt2[]' type='text' id='op' placeholder='Option2' required/>&nbsp;
                            <input name='opt3[]' type='text' id='op' placeholder='Option3' required/>&nbsp;
                            <input name='opt4[]' type='text' id='op' placeholder='Option4' required/>&nbsp;
                            <input name='ans[]' type='text' id='an' placeholder='Answer' required/></label></p>";
                            echo "<div id='div'></div>";
                            $que = $_POST["ques"];
                            $test = $_POST["testName"];
                            //echo $id;
                            for($i=0;$i<$que-1;$i++){
                                echo '<script type="text/javascript">',
                                     'generateRow();',
                                     '</script>';
                            }
                            echo "<br/>
                            <input type='submit' name='submit' id='submit' value='Submit' />
                    </p>
                    </form> 
                    </div>
                    </div>";
                if (isset($_POST['ques'])) {
                    $q= trim($_POST['ques']);
                    $testname = mysqli_real_escape_string($con,trim($_POST["testName"]));
                    //echo $q1." ".$op1." ".$op2." ".$op3." ".$op4." ".$an." ".$id."";
                    $qry="INSERT INTO testdetail VALUES (NULL,'$id', '$testname', '$q', '$q',NULL)";
                    $result = @mysqli_query($con,$qry);
                    if ($result) {
                        # code...
                        //echo "Executed";
                        //$s =
                    }
                    else {echo "Not Executed ".mysqli_error($con);}
                }
            }
            else if($table == "addtest"){
                echo "<form id='form1' name='form1' method='post' action='tables.php?id=addtest1&tab=".NULL."'>";
                    
                        echo "<div id='page-wrapper'>
                                <div class='row'>
                                    <!-- /.col-lg-12 -->
                                    <div class='col-lg-12'>
                                        <h1 class='page-header'>Test Details</h1>
                                    </div>
                                </div>
                                <!-- /.row -->
                                <div class='row'>
                                    <div class='col-lg-12'>";
                                        echo "<div class='panel panel-default'>
                                            <div class='panel-heading'>New Test <div class='navbar-right'><button type='button' id='tessubmit' onclick='history.go(-1);'>Back </button></div></div>
                                            <!-- /.panel-heading -->
                                            <div class='panel-body'>
                                                <div class='dataTable_wrapper'>
                                                    <table class='table table-striped table-bordered table-hover'>";
                                                $sql = "SELECT * FROM testdetail";
                                                $tid = mt_rand(1,1001);
                                                if($result = mysqli_query($con, $sql)){
                                                    while($row = mysqli_fetch_array($result)){
                                                        if($tid == $row['testid']){
                                                            $tid = mt_rand(1,1001);
                                                        }
                                                        else{$tid=$tid;}
                                                    }
                                                }
                                                mysqli_close($con);
                                                    echo "<input name='id' type='hidden' value='$tid'/>";
                                                    echo "<thead>";
                                                        echo "<tr>";
                                                            echo "<th>Test Name</th>";
                                                            echo "<th>No.of Question</th>";
                                                            echo "<th>Action</th>";
                                                        echo "</tr>";
                                                    echo "</thead>";
                                                    echo "<tbody>";
                                                        echo "<tr class='odd gradeX'>";
                                                            echo "<td class='center'><input name='testName' placeholder='Test Name' type='text' id='tes' required/></td>";
                                                            echo "<td class='center'><input name='ques' placeholder='No.Of Questions' type='text' id='no' required/></td>";
								                            echo "<td class='center'><input type='submit' name='submit' id='tessubmit' value='Submit' /></td>";
								                        echo "</tr>";
                                                    echo "</tbody>";
                                                echo "</table>";
                                            echo "</div>";
                                        echo "</div>";
                                    echo "</div>";
                                echo "</div>";
                            echo "</div>";
                        echo "</div>";    
                    
                echo "</form>";
            }
            else{
                if($table=="question"){
                    $sql = "SELECT * FROM $table";
                    if($result = mysqli_query($con, $sql)){
                        if(mysqli_num_rows($result) > 0){
                            $s=0;
                            echo "<div id='page-wrapper'>
                                <div class='row'>
                                    <!-- /.col-lg-12 -->
                                    <div class='col-lg-12'>
                                        <h1 class='page-header'>Question Details</h1>
                                    </div>
                                </div>
                                <!-- /.row -->
                                <div class='row'>
                                    <div class='col-lg-12'>";
                                        echo "<div class='panel panel-default'>
                                            <div class='panel-heading'>All questions <div class='navbar-right'><button type='button' id='tessubmit' onclick='history.go(-1);'>Back </button></div></div>
                                            <!-- /.panel-heading -->
                                            <div class='panel-body'>
                                                <div class='dataTable_wrapper'>
                                                    <table class='table table-striped table-bordered table-hover'>";
                                                        echo "<thead>";
                                                            echo "<tr>";
                                                                echo "<th>S No</th>";
                                                                echo "<th>Question</th>";
                                                                echo "<th>Choice 1</th>";
                                                                echo "<th>Choice 2</th>";
                                                                echo "<th>Choice 3</th>";
                                                                echo "<th>Choice 4</th>";
                                                                echo "<th>Answser</th>";
                                                                echo "<th>Edit</th>";
                                                                echo "<th>Delete</th>";
                                                            echo "</tr>";
                                                        echo "</thead>";
                                                        echo "<tbody>";
                                                        while($row = mysqli_fetch_array($result)){
                                                            $s +=1;
                                                            echo "<tr class='odd gradeX'>";
                                                                echo "<td class='center'>". $s ."</td>";
                                                                echo "<td class='center'>". $row['question'] ."</td>";
								                                echo "<td class='center'>". $row['ch1'] ."</td>";
								                                echo "<td class='center'>". $row['ch2'] ."</td>";
							        	                        echo "<td class='center'>". $row['ch3'] ."</td>";
                                                                echo "<td class='center'>". $row['ch4'] ."</td>";
                                                                echo "<td class='center'>". $row['ans'] ."</td>";
                                                                echo "<td class='center'>";
								                                    echo "<a href='tablestest.php?qid=". $row['sno'] ."&tid=".NULL."' title='Edit' data-toggle='tooltip'><span class='glyphicon glyphicon-ok-circle'>Edit</span></a>
                                                                </td>";
                                                                echo "<td class='center'>";
                                                                    echo "<a href='deleteQuestion.php?id=". $row['sno'] ."' title='Delete' data-toggle='tooltip'><span class='glyphicon glyphicon-ban-circle'>Delete</span></a>
                                                                </td>";
                                                            echo "</tr>";
                                                        }
                						                // Free result set
                                                        mysqli_free_result($result);
                                                        echo "</tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <div>";
                        }
                        else{
						echo "<p class='lead'><em>No records were found.</em></p>";
					    }
				    }
				    else{
					echo "ERROR: Could not able to execute $sql. " . mysqli_error($con);
				    }
                }
                else if($table=="login"){
                    $sql = "SELECT * FROM $table WHERE regno <>123 AND regno <>12345";
                    if($result = mysqli_query($con, $sql)){
                        if(mysqli_num_rows($result) > 0){
                            $s=0;
                            echo "<div id='page-wrapper'>
                                <div class='row'>
                                    <!-- /.col-lg-12 -->
                                    <div class='col-lg-12'>
                                        <h1 class='page-header'>View Users</h1>
                                    </div>
                                </div>
                                <!-- /.row -->
                                <div class='row'>
                                    <div class='col-lg-12'>";
                                        echo "<div class='panel panel-default'>
                                            <div class='panel-heading'>All Users <div class='navbar-right'><button type='button' id='tessubmit' onclick='history.go(-1);'>Back </button></div></div>
                                            <!-- /.panel-heading -->
                                            <div class='panel-body'>
                                                <div class='dataTable_wrapper'>
                                                    <table class='table table-striped table-bordered table-hover'>";
                                                        echo "<thead>";
                                                            echo "<tr>";
                                                                echo "<th>S No</th>";
                                                                echo "<th>Name</th>";
                                                                echo "<th>Register No</th>";
                                                                echo "<th>Department</th>";
                                                            echo "</tr>";
                                                        echo "</thead>";
                                                        echo "<tbody>";
                                                        while($row = mysqli_fetch_array($result)){
                                                            $s +=1;
                                                            echo "<tr class='odd gradeX'>";
                                                                echo "<td class='center'>". $s ."</td>";
                                                                echo "<td class='center'>". $row['name'] ."</td>";
                                                                echo "<td class='center'>". $row['regno'] ."</td>";
								                                echo "<td class='center'>". $row['dept'] ."</td>";
                                                            echo "</tr>";
                                                        }
                						                // Free result set
                                                        mysqli_free_result($result);
                                                        echo "</tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <div>";
                        }
                        else{
						echo "<p class='lead'><em>No records were found.</em></p>";
					    }
				    }
				    else{
					echo "ERROR: Could not able to execute $sql. " . mysqli_error($con);
				    }
                }
                else if($table=="testdetail"){
                    $sql = "SELECT * FROM $table";
                    if($result = mysqli_query($con, $sql)){
                        if(mysqli_num_rows($result) > 0){
                            $s=0;
                            echo "<div id='page-wrapper'>
                                <div class='row'>
                                    <!-- /.col-lg-12 -->
                                    <div class='col-lg-12'>
                                        <h1 class='page-header'>Test Details </h1>
                                    </div>
                                </div>
                                <!-- /.row -->
                                <div class='row'>
                                    <div class='col-lg-12'>";
                                        echo "<div class='panel panel-default'>
                                            <div class='panel-heading'>All Tests 
                                                <div class='navbar-right'>
                                                    <a href='tables.php?tab=addtest&id=".NULL."'><button type='button' id='tessubmit'>Add Test !</button></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                                    <button type='button' id='tessubmit' onclick='history.go(-1);'>Back </button>
                                                </div>
                                            </div>
                                            
                                            <!-- /.panel-heading -->
                                            <div class='panel-body'>
                                                <div class='dataTable_wrapper'>
                                                    <table class='table table-striped table-bordered table-hover' >";
                                                        echo "<thead>";
                                                            echo "<tr>";
                                                                echo "<th>S No</th>";
                                                                echo "<th>Test Name</th>";
                                                                echo "<th>Total Questions</th>";
                                                                echo "<th>Edit</th>";
                                                                echo "<th>Delete</th>";
                                                            echo "</tr>";
                                                        echo "</thead>";
                                                        echo "<tbody>";
                                                        while($row = mysqli_fetch_array($result)){
                                                            $s +=1;
                                                            echo "<tr class='odd gradeX'>";
                                                                echo "<td class='center'>". $s ."</td>";
                                                                echo "<td class='center'>". $row['testname'] ."</td>";
							    	                            echo "<td class='center'>". $row['questions'] ."</td>";
                                                                echo "<td class='center'>";
						            		                        echo "<a href='tablestest.php?tid=". $row['testid'] ."&qid=".NULL."' title='Edit' data-toggle='tooltip'><span class='glyphicon glyphicon-ok-circle'>Edit</span></a>
                                                                </td>";
                                                                echo "<td class='center'>";
                                                                    echo "<a href='deleteTest.php?id=". $row['testid'] ."' title='Delete' data-toggle='tooltip'><span class='glyphicon glyphicon-ban-circle'>Delete</span></a>
                                                                </td>";
                                                            echo "</tr>";
                                                        }
                						                // Free result set
                                                        mysqli_free_result($result);
                                                        echo "</tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <div>";
                        }
                        else{
						echo "<p class='lead'><em>No records were found.</em></p>";
					    }
				    }
				    else{
					echo "ERROR: Could not able to execute $sql. " . mysqli_error($con);
				    }
                }
                else if($table=="result"){
                    $sql = "SELECT * FROM $table WHERE regno <> 123 ORDER BY mark DESC";
                    if($result = mysqli_query($con, $sql)){
                        if(mysqli_num_rows($result) > 0){
                            $s=0;
                            echo "<div id='page-wrapper'>
                                <div class='row'>
                                    <!-- /.col-lg-12 -->
                                    <div class='col-lg-12'>
                                        <h1 class='page-header'>Test Taken Details</h1>
                                    </div>
                                </div>
                                <!-- /.row -->
                                <div class='row'>
                                    <div class='col-lg-12'>";
                                        echo "<div class='panel panel-default'>
                                            <div class='panel-heading'>Top Result <div class='navbar-right'><button type='button' id='tessubmit' onclick='history.go(-1);'>Back </button></div></div>
                                            <!-- /.panel-heading -->
                                            <div class='panel-body'>
                                                <div class='dataTable_wrapper'>
                                                    <table class='table table-striped table-bordered table-hover' >";
                                                        echo "<thead>";
                                                            echo "<tr>";
                                                                echo "<th>S No</th>";
                                                                echo "<th>Name</th>";
                                                                echo "<th>Test Name</th>";
                                                                echo "<th>Mark</th>";
                                                            echo "</tr>";
                                                        echo "</thead>";
                                                        echo "<tbody>";
                                                        while($row = mysqli_fetch_array($result)){
                                                            $s +=1;
                                                            $reg = $row['regno'];
                                                            $id = $row['testid'];
                                                            $sql1 = "SELECT * FROM login WHERE regno = $reg";
                                                            if($result1 = mysqli_query($con, $sql1)){
                                                                if(mysqli_num_rows($result1) > 0){
                                                                    while($row1 = mysqli_fetch_array($result1)){
                                                                        $names = $row1['name']; 
                                                                    }
                                                                }
                                                            }
                                                            $sql2 = "SELECT * FROM testdetail WHERE testid = $id";
                                                            if($result2 = mysqli_query($con, $sql2)){
                                                                if(mysqli_num_rows($result2) > 0){
                                                                    while($row2 = mysqli_fetch_array($result2)){
                                                                        $testnames = $row2['testname']; 
                                                                    }
                                                                }
                                                            }
                                                            echo "<tr class='odd gradeX'>";
                                                                echo "<td class='center'>". $s ."</td>";
                                                                echo "<td class='center'>". strtoupper($names) ."</td>";
                                                                echo "<td class='center'>". strtoupper($testnames) ."</td>";
                                                                echo "<td class='center'>". $row['mark'] ."</td>";
                                                            echo "</tr>";
                                                        }
                                                        // Free result set
                                                        mysqli_free_result($result);
                                                        echo "</tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <div>";
                        }
                        else{
						echo "<p class='lead'><em>No records were found.</em></p>";
					    }
				    }
				    else{
					echo "ERROR: Could not able to execute $sql. " . mysqli_error($con);
				    }
                }

				// Close connection
				mysqli_close($con);
            }
            ?>
        <!-- jQuery -->
        <script src="../js/jquery.min.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="../js/bootstrap.min.js"></script>

        <!-- Metis Menu Plugin JavaScript -->
        <script src="../js/metisMenu.min.js"></script>

        <!-- DataTables JavaScript -->
        <script src="../js/dataTables/jquery.dataTables.min.js"></script>
        <script src="../js/dataTables/dataTables.bootstrap.min.js"></script>

        <!-- Custom Theme JavaScript -->
        <script src="../js/startmin.js"></script>

        <!-- Page-Level Demo Scripts - Tables - Use for reference -->
        <script>
            $(document).ready(function() {
                $('#dataTables-example').DataTable({
                        responsive: true
                });
            });
        </script>

    </body>
</html>

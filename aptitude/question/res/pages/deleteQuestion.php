<?php

  //get TestTopicName.....Notification start
     $sql1 = "SELECT * FROM testdetail where testid = $id";
                if($result1 = mysqli_query($con, $sql1)){
                    if(mysqli_num_rows($result1) > 0){
                        while($row1 = mysqli_fetch_array($result1)){
                            $testname = $row1['testname']; 
                        }
                    }
                }


// Process delete operation after confirmation
if(isset($_GET["id"]) && !empty($_GET["id"])){
    // Include config file
    require_once 'config.php';
    $sno = $_GET["id"];

    // Prepare a select statement
    //$sql = "DELETE FROM testdetail WHERE testid = ?";
    $sql = "DELETE FROM question WHERE sno = ?";
    if($stmt = mysqli_prepare($link, $sql)){
        // Bind variables to the prepared statement as parameters
        mysqli_stmt_bind_param($stmt, "i", $param_id);
        
        // Set parameters
        $param_id = trim($_GET["id"]);
        
        // Attempt to execute the prepared statement
        if(mysqli_stmt_execute($stmt)){
             //Notification Code

$message = array("topic" => "Test Deleted",
                 "title" => "$testname"
                 );

$url = 'https://fcm.googleapis.com/fcm/send';
$fields = array(
    //'registration_ids' => $tokens, //tokens
    //"condition" => "'dogs' in topics || 'cats' in topics",
    "to" => "/topics/aptitude",
    'data' => $message
);

$headers = array('Content-Type: application/json',
    'Authorization:key=AAAARErXBaA:APA91bEzOhYCjwD3UcCXN8ZHFBChBBrjgJi65--nZ4zmB0kgSMo1huV9En-YXT9BhSJjh0DE0zOYXporBrQeFirEAtthNQ-7NkcMvPIWMnxycO2V4FNaY-_z87_xDL-OeCScJNM2SzPu'
);

$ch = curl_init($url);
curl_setopt($ch, CURLOPT_POST, true);

curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
$result = curl_exec($ch);
/*
if ($result == FALSE)
    die('Curl failed: ' . curl_error($ch));

curl_close($ch);
$conn->close();*/

$response = curl_exec($ch);
$err = curl_error($ch);

curl_close($ch);

if ($err) {
  echo "cURL Error #:" . $err;
} else {
  echo $response;
}

//Notification end
            
            
            // Records deleted successfully. Redirect to landing page
            Header( "HTTP/1.1 301 Moved Permanently" );
            echo "<script language='javascript' type='text/javascript'>";
            echo "window.location.href='tables.php?tab=question&id=".NULL."'";
            echo "</script>"; 
            exit();
        }
        else{
            Header( "HTTP/1.1 301 Moved Permanently" );
            echo "<script language='javascript' type='text/javascript'>";
            echo "alert('Oops delete fail! Something went wrong. Please try again later.');";
            echo "window.location.href='tables.php?tab=question&id=".NULL."'";
            echo "</script>";
        }
    }     
    // Close statement
    mysqli_stmt_close($stmt);
} else{
    // Check existence of id parameter
    if(empty($_GET["id"])){
        // URL doesn't contain id parameter. Redirect to error page
        Header( "HTTP/1.1 301 Moved Permanently" );
        echo "<script language='javascript' type='text/javascript'>";
        echo "alert('Oops delete fail! Something went wrong. Please try again later.');";
        echo "window.location.href='../error.php'";
        echo "</script>";
        exit();
    }
}
?>
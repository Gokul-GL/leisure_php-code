<?php
	// Include config file
	require_once 'config.php';
    $totalquestions=0;                
    // Attempt select query execution
    $sql = "SELECT * FROM question";
    if($result = mysqli_query($con, $sql)){
        if(mysqli_num_rows($result) > 0){
            while($row = mysqli_fetch_array($result)){
                $totalquestions +=1;
            }
            mysqli_free_result($result);
        }
    }
    $totaltests=0;                
    // Attempt select query execution
    $sql = "SELECT * FROM testdetail";
    if($result = mysqli_query($con, $sql)){
        if(mysqli_num_rows($result) > 0){
            while($row = mysqli_fetch_array($result)){
                $totaltests +=1;
            }
            mysqli_free_result($result);
        }
    }

    $todaytest=0;                
    // Attempt select query execution
    $date = date("Y-m-d");
    $sql = "SELECT * FROM result WHERE testtime LIKE '$date%'";
   
    if($result = mysqli_query($con, $sql)){
        if(mysqli_num_rows($result) > 0){
            while($row = mysqli_fetch_array($result)){
                $todaytest +=1;
            }
            mysqli_free_result($result);
        }
    }

    $monthuser=0;                
   //Our SQL statement.
   $sql = "SELECT * FROM login WHERE MONTH(authtime) = MONTH(CURRENT_DATE()) AND YEAR(authtime) = YEAR(CURRENT_DATE())";
    if($result = mysqli_query($con, $sql)){
        if(mysqli_num_rows($result) > 0){
            while($row = mysqli_fetch_array($result)){
                $monthuser +=1;
            }
            mysqli_free_result($result);
        }
    }

    $monthques=0;                
   //Our SQL statement.
   $sql = "SELECT * FROM question WHERE MONTH(addtime) = MONTH(CURRENT_DATE()) AND YEAR(addtime) = YEAR(CURRENT_DATE())";
    if($result = mysqli_query($con, $sql)){
        if(mysqli_num_rows($result) > 0){
            while($row = mysqli_fetch_array($result)){
                $monthques +=1;
            }
            mysqli_free_result($result);
        }
    }

    $monthtest=0;                
   //Our SQL statement.
   $sql = "SELECT * FROM testdetail WHERE MONTH(edittime) = MONTH(CURRENT_DATE()) AND YEAR(edittime) = YEAR(CURRENT_DATE())";
    if($result = mysqli_query($con, $sql)){
        if(mysqli_num_rows($result) > 0){
            while($row = mysqli_fetch_array($result)){
                $monthtest +=1;
            }
            mysqli_free_result($result);
        }
    }

    $largestNumber;
    $sql = "SELECT MAX(mark) AS maxnum FROM result";
   if($result = mysqli_query($con, $sql)){
        if(mysqli_num_rows($result) > 0){
            while($row = mysqli_fetch_array($result)){
                $largestNumber = $row['maxnum'];
            }
            mysqli_free_result($result);
        }
    }

    $totalusers=0;                
    // Attempt select query execution
    $sql = "SELECT * FROM login WHERE regno <>123 AND regno <>12345";
    if($result = mysqli_query($con, $sql)){
        if(mysqli_num_rows($result) > 0){
            while($row = mysqli_fetch_array($result)){
                $totalusers +=1;
            }
            mysqli_free_result($result);
        }
    }

     $newreq=0;                
    // Attempt select query execution
    $sql = "SELECT * FROM register";
    if($result = mysqli_query($con, $sql)){
        if(mysqli_num_rows($result) > 0){
            while($row = mysqli_fetch_array($result)){
                $newreq +=1;
            }
            mysqli_free_result($result);
        }
    }

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Admin Panel</title>

        <!-- Bootstrap Core CSS -->
        <link href="../css/bootstrap.min.css" rel="stylesheet">

        <!-- MetisMenu CSS -->
        <link href="../css/metisMenu.min.css" rel="stylesheet">

        <!-- Timeline CSS -->
        <link href="../css/timeline.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="../css/startmin.css" rel="stylesheet">

        <!-- Morris Charts CSS -->
        <link href="../css/morris.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="../css/style.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>

        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                
                <ul class="nav navbar-nav navbar-left navbar-top-links">
                    <li><a href="//www.sasurie.com"><i class="fa fa-home fa-fw"></i> Website</a></li>
                    
                </ul>

                <ul class="nav navbar-nav navbar-right navbar-top-links">
      
                            <li class="divider"><a class="navbar-brand" >Admin</a></li>
                            <li><a href="logout.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                      
                       
                </ul>
                <!-- /.navbar-top-links -->

                <div class="navbar-default sidebar" role="navigation">
                
                    <div class="sidebar-nav navbar-collapse">
                        <ul class="nav" id="side-menu">
                            <li class="sidebar-search">
                                <div class="input-group custom-search-form">
                                    <img src="../images/logo.png">
                                                                    
                                </div>
                                <!-- /input-group -->
                            </li>
                       
                            <li>
                                <a href="index.php"><i class="fa fa-laptop fa-fw"></i> Dashboard</a>
                            </li>
                            <li>
                                <a href="tables.php?tab=question&amp;id=<?php echo NULL;?>"><i class="fa fa-question-circle-o fa-fw"></i> Question</a>
                            </li>
                            <li>
                                <a href="tables.php?tab=testdetail&amp;id=<?php echo NULL;?>"><i class="fa fa-pencil-square-o fa-fw"></i> Tests</a>
                            </li>
                            <li>
                                <a href="tables.php?tab=result&amp;id=<?php echo NULL;?>"><i class="fa fa-hand-o-up fa-fw"></i> Results</a>
                            </li>
                            <li>
                                <a href="tables.php?tab=login&amp;id=<?php echo NULL;?>"><i class="fa fa-users fa-fw"></i> Users</a>
                            </li>
                            <li>
                                <a href="viewdetail.php?view=request"><i class="fa fa-registered fa-fw"></i> New Request</a>
                            </li>
                            <li>
                                <a href="tables.php?tab=addtest&amp;id=<?php echo NULL;?>"><i class="fa fa-plus-square-o fa-fw"></i> Add Test</a>
                            </li>
                            <li>
                                <a href="update.php"><i class="fa fa-upload fa-fw"></i> Update</a>
                            </li>
                            
                        </ul>
                    </div>
                </div>
            </nav>

            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header-dash">Dashboard</h1>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <div class="row">
                    <div class="col-lg-3 col-md-6">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-question-circle-o fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="huge"><?php echo $totalquestions;?></div>
                                        <div>Total Question !</div>
                                    </div>
                                </div>
                            </div>
                            <a href="tables.php?tab=question&amp;id=<?php echo NULL;?>">
                                <div class="panel-footer">
                                    <span class="pull-left">View Details</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>

                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="panel panel-green">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-pencil-square-o fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="huge"><?php echo $totaltests;?></div>
                                        <div>Total Tests !</div>
                                    </div>
                                </div>
                            </div>
                            <a href="tables.php?tab=testdetail&amp;id=<?php echo NULL;?>">
                                <div class="panel-footer">
                                    <span class="pull-left">View Details</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>

                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                    
                    <div class="col-lg-3 col-md-6">
                        <div class="panel panel-red">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-hand-o-up fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="huge"><?php echo $largestNumber;?></div>
                                        <div>Top Score !</div>
                                    </div>
                                </div>
                            </div>
                            <a href="tables.php?tab=result&amp;id=<?php echo NULL;?>">
                                <div class="panel-footer">
                                    <span class="pull-left">View Details</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>

                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-6">
                        <div class="panel panel-yellow">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-users fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="huge"><?php echo $totalusers;?></div>
                                        <div>Total Users !</div>
                                    </div>
                                </div>
                            </div>
                            <a href="tables.php?tab=login&amp;id=<?php echo NULL;?>">
                                <div class="panel-footer">
                                    <span class="pull-left">View Details</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>

                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>

                    <div>&nbsp;</div>
                    <div>&nbsp;</div>

                    <div class="col-lg-3 col-md-6">
                        <div class="panel panel-green">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-puzzle-piece fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="huge"><?php echo $todaytest;?></div>
                                        <div>Today's Test Taken !</div>
                                    </div>
                                </div>
                            </div>
                            <a href="viewdetail.php?view=todaytest">
                                <div class="panel-footer">
                                    <span class="pull-left">View Details</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-6">
                        <div class="panel panel-red">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-registered fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="huge"><?php echo $newreq;?></div>
                                        <div>New Request's !</div>
                                    </div>
                                </div>
                            </div>
                            <a href="viewdetail.php?view=request">
                                <div class="panel-footer">
                                    <span class="pull-left">View Details</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                    
                    <div class="col-lg-3 col-md-6">
                        <div class="panel panel-yellow">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-user-plus fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="huge"><?php echo $monthuser;?></div>
                                        <div>Month added Users  !</div>
                                    </div>
                                </div>
                            </div>
                            <a href="viewdetail.php?view=monthuser">
                                <div class="panel-footer">
                                    <span class="pull-left">View Details</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-6">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-plus-square-o fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="huge"><?php echo $monthques;?></div>
                                        <div>Month Question !</div>
                                    </div>
                                </div>
                            </div>
                            <a href="viewdetail.php?view=monthquestion">
                                <div class="panel-footer">
                                    <span class="pull-left">View Details</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                    
                    <div>&nbsp;</div>
                    <div>&nbsp;</div>

                    <div class="col-lg-3 col-md-6">
                        <div class="panel panel-red">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-puzzle-piece fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="huge"><?php echo $monthtest;?></div>
                                        <div>Month added Test !</div>
                                    </div>
                                </div>
                            </div>
                            <a href="viewdetail.php?view=monthtest">
                                <div class="panel-footer">
                                    <span class="pull-left">View Details</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /#page-wrapper -->
        </div>
        <!-- /#wrapper -->
        <!-- jQuery -->
        <script src="../js/jquery.min.js"></script>
        <!-- Bootstrap Core JavaScript -->
        <script src="../js/bootstrap.min.js"></script>
        <!-- Metis Menu Plugin JavaScript -->
        <script src="../js/metisMenu.min.js"></script>
        <!-- Morris Charts JavaScript -->
        <script src="../js/raphael.min.js"></script>
        <script src="../js/morris.min.js"></script>
        <script src="../js/morris-data.js"></script>
        <!-- Custom Theme JavaScript -->
        <script src="../js/startmin.js"></script>
    </body>
</html>

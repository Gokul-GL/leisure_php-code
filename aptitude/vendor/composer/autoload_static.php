<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitfc970340168011b2daef54d7e131ee2d
{
    public static $prefixLengthsPsr4 = array (
        'F' => 
        array (
            'Firebase\\JWT\\' => 13,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Firebase\\JWT\\' => 
        array (
            0 => __DIR__ . '/..' . '/firebase/php-jwt/src',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInitfc970340168011b2daef54d7e131ee2d::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInitfc970340168011b2daef54d7e131ee2d::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
